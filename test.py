from StatisticsComputer import *

def main():
    try:
        # classifier = Classifier(train_range='1-5', test_range='6', results_folder='cifar10-results')
        classifier = Classifier(train_range='1-2', test_range='3', results_folder='resized-inria-results')
        # classifier = Classifier(train_range='1-2', test_range='3', results_folder='veryfast-inria-results')
        # classifier = Classifier(train_range='1-2', test_range='3', results_folder='veryfast-subinria-results')

        statistics_computer = StatisticsComputer(classifier)

        # estimates = statistics_computer.compute(training_data=True)
        estimates = statistics_computer.compute(testing_data=True)
        print estimates
    except ValueError as error:
        print 'ValueError excepted: ',error.message
    except RuntimeError as error:
        print 'RuntimeError excepted: ',error.message

if __name__ == "__main__":
    main()