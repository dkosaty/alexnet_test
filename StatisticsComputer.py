import numpy as np, re, cPickle
from Classifier import *

class StatisticsComputer:

    def __init__(self, classifier):
        self.classifier = classifier
        self.estimates = Estimates()

    @staticmethod
    def parse_range(scale):
        pattern = r'(\d+)-?(\d*)'
        founded = re.search(pattern, scale)
        if founded:
            if founded.endpos == 3:
                first, last = int(founded.group(1)), int(founded.group(2))
            else:
                first = int(founded.group(1))
                last = first
            batch_names = ['data_batch_'+str(num) for num in range(first, last+1)]
        else:
            raise ValueError, "don't parse the ranges."
        return batch_names, last-first

    def compute(self, training_data=False, testing_data=False):
        if training_data:
            scale = self.classifier.train_range
            batches_list, batches_number = StatisticsComputer.parse_range(scale)
        elif testing_data:
            scale = self.classifier.test_range
            batches_list, batches_number = StatisticsComputer.parse_range(scale)
        else:
            raise ValueError, "don't parse the range."

        os.system\
            (
                'python ~/cuda-convnet-master/shownet.py '
                '-f %s '
                '--write-features=probs '
                '--feature-path=%s '
                '--test-range=%s'
                % (self.classifier.trained_cascade, self.classifier.results_folder, scale)
            )

        real_labels, predicted_labels = [], []

        for batch in batches_list:
            data = cPickle.load(open(os.path.join(self.classifier.results_folder, batch), 'rb'))

            label = np.argmax(data['data'], axis=1)
            predicted_labels += label.tolist()

            label = np.asarray(data['labels'], dtype=np.int)
            real_labels += label[0].tolist()

        self.estimates.evaluate(real_labels=real_labels, predicted_labels=predicted_labels)

        return self.estimates

class Estimates:

    def __init__(self):
        self.true_positive = 0
        self.false_positive = 0
        self.true_negative = 0
        self.false_negative = 0

        self.precision = 0
        self.recall = 0
        self.accuracy = 0

    def evaluate(self, real_labels, predicted_labels):
        validation_data_size = len(real_labels)

        print 'validation data size =',validation_data_size

        # for i in range(N):
        #     print i+1, real_labels[i], predicted_labels[i]

        for i in range(validation_data_size):
            if real_labels[i] == predicted_labels[i]:
                if predicted_labels[i] == 1:
                    self.true_positive += 1
                else:
                    self.true_negative += 1
            else:
                if predicted_labels[i] == 1:
                    self.false_positive += 1
                else:
                    self.false_negative += 1

        TP = self.true_positive
        FP = self.false_positive
        TN = self.true_negative
        FN = self.false_negative

        self.precision = TP / float(TP + FP) * 100
        self.recall = TP / float(TP + FN) * 100
        self.accuracy = (TP + TN) / float(TP + TN + FP + FN) * 100

    def __str__(self):
        return str('TP = %d,  FP = %d, TN = %d, FN = %d' % \
              (self.true_positive, self.false_positive, self.true_negative, self.false_negative)) + '\n' + \
               str('precision = %f, recall = %f, accuracy = %f' % (self.precision, self.recall, self.accuracy))