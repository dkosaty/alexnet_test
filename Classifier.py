import os

class Classifier:

    def __init__(self, train_range, test_range, results_folder, layers=None, layer_params=None, batches_folder=None, epochs=None):
        self.results_folder = os.path.join('/home/dmitry/PycharmProjects/alexnet_test', results_folder)
        if not os.path.exists(self.results_folder):
            os.makedirs(self.results_folder)

        self.trained_cascade = self.get_cascade()

        self.train_range, self.test_range = train_range, test_range

        self.provider = 'inria-cropped'

        self.layers = layers
        self.layer_params = layer_params

        self.batches_folder = batches_folder

        self.epochs = epochs

    def get_cascade(self):
        trained_cascade = None

        if os.path.exists(self.results_folder):
            content = filter(lambda x: x.startswith('ConvNet'), os.listdir(self.results_folder))
            if content:
                trained_cascade = os.path.join(self.results_folder, content[0])
        return trained_cascade

    def train(self, start=False, proceed=False):
        if start:
            os.system\
                (
                    'python ~/cuda-convnet-master/convnet.py '
                    '--data-path=%s '
                    '--save-path=%s '
                    '--train-range=%s '
                    '--test-range=%s '
                    '--layer-def=%s '
                    '--layer-params=%s '
                    '--data-provider=%s '
                    '--test-freq=13 '
                    '--epochs=%s'
                    % (self.batches_folder,
                       self.results_folder,
                       self.train_range,
                       self.test_range,
                       self.layers,
                       self.layer_params,
                       self.provider,
                       self.epochs)
                )
        elif proceed:
            if self.trained_cascade is None:
                raise ValueError, 'missing the trained cascade.'

            os.system\
                (
                    'python ~/cuda-convnet-master/convnet.py '
                    '-f %s '
                    '--epochs=%s' % (self.trained_cascade, self.epochs)
                )

    def demonstrate(self):
        if self.trained_cascade is None:
            raise ValueError, 'missing the trained cascade.'

        os.system\
            (
                'python ~/cuda-convnet-master/shownet.py '
                '-f %s '
                '--show-preds=probs' % self.trained_cascade
            )

    def view_learned_filters(self, layer):
        os.system\
            (
                'python ~/cuda-convnet-master/shownet.py '
                '-f %s '
                '--show-filters=%s '
                # '--channels=3'
                % (self.trained_cascade, layer)
            )