from StatisticsComputer import *

def main():
    # classifier = Classifier\
    #     (
    #         train_range='1-5',
    #         test_range='6',
    #         results_folder='cifar10-results',
    #         layers='layers-19pct.cfg',
    #         layer_params='layer-params-19pct.cfg',
    #         batches_folder='cifar-10-py-colmajor',
    #         epochs=3
    #     )

    classifier = Classifier\
        (
            train_range='1-2',
            test_range='3',
            batches_folder='inria-batches-resized-to32x32',
            layers='layers.cfg',
            layer_params='layer-params.cfg',
            results_folder='resized-inria-results',
            epochs=1100
        )

    # classifier = Classifier\
    #     (
    #         train_range='1-2',
    #         test_range='3',
    #         batches_folder='inria-batches-after-veryfast',
    #         layers='layers.cfg',
    #         layer_params='layer-params.cfg',
    #         results_folder='veryfast-inria-results',
    #         epochs=100
    #     )

    # classifier = Classifier\
    #     (
    #         train_range='1-2',
    #         test_range='3',
    #         batches_folder='subinria-batches-after-veryfast',
    #         layers='layers.cfg',
    #         layer_params='layer-params.cfg',
    #         results_folder='veryfast-subinria-results',
    #         epochs=1500
    #     )

    # classifier.train(start=True)
    classifier.train(proceed=True)

    # classifier.demonstrate()

    # classifier.view_learned_filters('conv1')
    # classifier.view_learned_filters('conv2')
    # classifier.view_learned_filters('conv3')

if __name__ == "__main__":
    main()